import {
  extend,
  configure
} from "vee-validate";
import {
  required,
  email
} from "vee-validate/dist/rules";

extend("required", required);
extend("email", email);

export default function VeeValidatePlugin({
  app
}) {
  configure({
    // this will be used to generate messages.
    defaultMessage: (field, values) => {
      // override the field name.
      values._field_ = app.i18n.t(`validations.fields.${field}`);
      return app.i18n.t(`validations.messages.${values._rule_}`, values);
    }
  });
}

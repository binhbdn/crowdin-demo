export default {
  'meta': { 'description': '素晴らしい鮮度フラワーショップ' },
  'nav': {
    'home': 'ホーム',
    'about': 'について',
    'service': 'サービス',
    'product': '製品',
    'showAllProducts': 'すべての製品',
    'orderProducts': '製品を注文する',
    'cart': 'カート',
    'checkout': 'チェックアウト',
    'wishList': 'ほしい物リスト',
    'searchProduct': '製品検索',
    'productDecoCorner': '製品デココーナー',
    'blog': 'ブログ（英語のみ）',
    'contact': 'お問い合わせ',
    'account': 'アカウント',
    'register': '登録',
    'signIn': 'サインイン',
    'signOut': 'サインアウト',
    'dashboard': 'ダッシュボード'
  }
};
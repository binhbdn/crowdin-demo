export default {
  "meta": {
    "description": "Amazing freshness flower shop",
  },

  "nav": {
    "home": "Home",
    "about": "About",
    "service": "Services",
    "product": "Product",
    "showAllProducts": "Show all products",
    "orderProducts": "Order products",
    "cart": "Cart",
    "checkout": "Checkout",
    "wishList": "Wishlist",
    "searchProduct": "Search product",
    "productDecoCorner": "Product deco corner",
    "blog": "Blog",
    "contact": "Contact",
    "account": "Account",
    "register": "Register",
    "signIn": "Sign in",
    "signOut": "Sign out",
    "dashboard": "Dashboard",
  },
}
